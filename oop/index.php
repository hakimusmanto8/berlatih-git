<?php

    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("shaun");

    echo $sheep->name.'<br/>'; // "shaun"
    echo $sheep->legs.'<br/>'; // 2
    echo $sheep->cold_blooded.'<br/>'; // false

    $kodok = new Frog("buduk");
    echo $kodok->name.'<br/>';
    echo $kodok->legs.'<br/>';
    echo $kodok->cold_blooded.'<br/>';
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo $sungokong->name.'<br/>';
    echo $sungokong->legs.'<br/>';
    echo $sungokong->cold_blooded.'<br/>';
    $sungokong->yell() // "Auooo"
?>
